Zadání
=========

Programování – hadi a žebříky
----------------------------------

Vytvořte jednoduchou hru. 
Na vstupu bude počet hráčů.
Herní deska vypadá takto:

[![](desk.png)](hadi.pdf)

### Pravidla

* Každý hráč hodí kostkou, posune se o hozený počet polí.
    Pokud padne 6 hází znovu a hody se sečtou.
* Pokud ukončí tah na poli s žebříkem, posune na se pole s koncem žebříku.
    Pouze směrem nahoru.
* Pokud ukončí tah na poli s hlavou hada, posune na se pole s ocasem hada.
* Pokud hráč skončí na políčku s jinou figurkou posune ji o jedno pole zpět.
    Na figurku se aplikují pravidla jako by na pole právě přišla 
    (žebříky, hadi, jiná figurka)
* Hráč, který první dorazí na pole 100 vyhrál. Je potřeba skončit přesně
    na daném poli. Pokud by měl pole překročit tah neproběhne.

Doplňte prosím o textové výpisy, aby bylo možné hru sledovat. Např. „Hráč A hodil 5 a posunul se na
pole 8“.
